var casper = require('casper').create({
    viewportSize: {width: 1024, height: 768}
});
var passed = false;

casper.start('http://casperjs.org/', function() {
    this.echo(this.getTitle());
});

casper.thenOpen('http://phantomjs.org', function() {
    this.echo(this.getTitle());
    window.setTimeout(function() { passed = true; }, 10000);
});

casper.waitFor(function() { return passed; }, function() {}, function() {}, 15000);

casper.run();
