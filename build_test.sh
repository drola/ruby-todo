#!/bin/bash
export ENV="test"
export PORT="7331"
# Use the correct ruby
source ~/.rvm/scripts/rvm
rvm use default
rvm install ruby --latest

# Set "fail on error" in bash
set -e

bundle install

gem install --no-rdoc --no-ri rails


# Do any setup
rake db:migrate

#Run your tests
#rake
#Start server

rails server -e $ENV -p $PORT &


#casperjs test/integration/test1.js

./maximize_browser_window.sh &
casperjs --engine=slimerjs test/integration/test1.js
./maximize_browser_window.sh &
TEST_RESULTS_DIR=${WORKSPACE}/test_results
[ ! -d $TEST_RESULTS_DIR ] && mkdir -p $TEST_RESULTS_DIR
casperjs test test/integration/test2.js  --xunit=${WORKSPACE}/test_results/log.xml --engine=slimerjs

if [ -f tmp/pids/server.pid ] && [ -s tmp/pids/server.pid ]
 then
    kill `cat tmp/pids/server.pid`
fi