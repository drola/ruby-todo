#!/bin/bash
until ./fetch_browser_window_id.sh | grep -m 1 "0x"; do : sleep 1s ; done
sleep 0.1s
wmctrl -i -r `./fetch_browser_window_id.sh` -b toggle,maximized_vert,maximized_horz
wmctrl -i -r `./fetch_browser_window_id.sh` -b add,above
